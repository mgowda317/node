public class LookCommand implements Command {
    private Player player;

    public LookCommand() {
        this.player = Graph.getInstance().getPlayer();
    }

    @Override
    public void init(String userString) {

    }

    @Override
    public boolean execute() {
        for (Entity e : player.getRoom().getEntities()) {
            System.out.println(e.getName());
        }
        return false;
    }

    @Override
    public String getCommandWord() {
        return "look";
    }
}

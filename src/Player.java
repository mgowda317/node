public class Player extends GenericEntity implements Observer {
    private String name, description;
    public ItemContainer items;


    public Player(String name, String description, Graph.Node currentRoom) {
        super(currentRoom);
        setName(name);
        setDescription(description);
        items = new ItemContainer();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInventoryString() {
        return items.getInventoryString();
    }


    public void pickItem(String itemName) {
        Graph.Node room = getRoom();
        Item item = room.removeItem(itemName);
        if (item == null) {
            System.out.println(itemName + " is not an item in the " + room);
        } else {
            items.addItem(item);
            System.out.println("You have picked up the: " + itemName);
        }
    }

    @Override
    public void move() {
        System.out.println("Player does not know how to move without a destination");
    }

    public void move(String rooName) {
        Graph.Node neighbor = getNeighbor(rooName);
        if (neighbor != null) {
            setRoom(neighbor);
        }
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public Graph.Node getNeighbor(String roomName) {
        return getRoom().getNeighbor(roomName);
    }

    public void addNeighbor(String name) {
        getRoom().addNeighbor(name);
    }

    @Override
    public void handleRoomMoveNotification(Graph.Node room) {
        if (items.contains("wumpus tracker")) {
            System.out.println("The wumpus moved to " + room.getName());
        }
    }
}

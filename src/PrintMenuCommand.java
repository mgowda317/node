public class PrintMenuCommand implements Command{
    private Player player;

    public PrintMenuCommand() {
        this.player = Graph.getInstance().getPlayer();
    }

    @Override
    public void init(String userString) {

    }

    @Override
    public boolean execute() {
        System.out.println("Your commands are:");
        System.out.println("go <room name>");
        System.out.println("look");
        System.out.println("add room <room name>");
        System.out.println("commands");
        System.out.println("take <item name>");
        System.out.println("drop <item name>");
        System.out.println("quit");
        return false;
    }

    @Override
    public String getCommandWord() {
        return "print";
    }
}

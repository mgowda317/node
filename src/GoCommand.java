public class GoCommand implements Command {
    private Player player;
    private String roomName;

    public GoCommand() {
        this.player = Graph.getInstance().getPlayer();
    }

    public String subtractCommandWord(String userString) {
        String subtracter = getCommandWord();
        int lengthSubtracter = subtracter.length();
        String finalString = userString.substring(lengthSubtracter);
        return finalString;
    }


    @Override
    public void init(String userString) {
        this.roomName = subtractCommandWord(userString).trim();
    }

    @Override
    public boolean execute() {
        player.move(roomName);
        return false;
    }

    @Override
    public String getCommandWord() {
        return "go";
    }
}

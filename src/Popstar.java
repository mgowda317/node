public class Popstar extends GenericEntity {
    public void move() {
        Player player = Graph.getInstance().getPlayer();
        Graph.Node playerRoom = player.getRoom();
        if (getRoom().getNeighbors().contains(playerRoom)) {
            setRoom(playerRoom);
        } else {
            randomMove();
        }
    }

    public Popstar(String name, String description, Graph.Node currentRoom) {
        super(currentRoom);
        setName(name);
        setDescription(description);
    }
}

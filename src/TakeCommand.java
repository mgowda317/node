public class TakeCommand implements Command {
    private Player player;
    private String itemName;

    public TakeCommand() {
        this.player = Graph.getInstance().getPlayer();
    }

    public String subtractCommandWord(String userString) {
        String subtracter = getCommandWord();
        int lengthSubtracter = subtracter.length();
        String finalString = userString.substring(lengthSubtracter);
        return finalString;
    }

    @Override
    public void init(String userString) {
        this.itemName = subtractCommandWord(userString).trim();
    }

    @Override
    public boolean execute() {
        player.pickItem(itemName);
        return false;
    }

    @Override
    public String getCommandWord() {
        return "take";
    }
}

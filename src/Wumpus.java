import java.util.ArrayList;
import java.util.Random;

public class Wumpus extends GenericEntity implements Observable {
    ArrayList<Observer> observers;

    public void move() {
        Player player = Graph.getInstance().getPlayer();
        Graph.Node playerRoom = player.getRoom();
        if (getRoom().getNeighbors().contains(playerRoom)) {
            ArrayList<Graph.Node> neighbors = (ArrayList<Graph.Node>) (getRoom().getNeighbors().clone());
            neighbors.remove(playerRoom);
            int neighborsCount = neighbors.size();
            if (neighborsCount == 0) {
                return;
            } else {
                Random rand = new Random();
                int neighborsIndex = rand.nextInt(neighborsCount);
                Graph.Node nextRoom = neighbors.get(neighborsIndex);
                setRoom(nextRoom);
            }
        } else {
            randomMove();
        }
        notifier();
    }

    public void notifier() {
        for (Observer observer : observers) {
            observer.handleRoomMoveNotification(getRoom());
        }
    }


    public Wumpus(String name, String description, Graph.Node currentRoom) {
        super(currentRoom);
        setName(name);
        setDescription(description);
        observers = new ArrayList<Observer>();
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }
}

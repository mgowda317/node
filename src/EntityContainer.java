import java.util.ArrayList;
import java.util.List;

public class EntityContainer {
    private List<Entity> entities;

    public List<Entity> getEntities() {
        return entities;
    }

    public EntityContainer() {
        entities = new ArrayList<Entity>();
    }

    public void addEntity(Entity e) {
        entities.add(e);
    }
}

public interface Entity {
    public String getName();

    public void move();

    public Graph.Node getRoom();

    public void setRoom(Graph.Node node);
}
public class AddCommand implements Command {
    private Player player;
    private String addedRoomName;

    public AddCommand() {
        this.player = Graph.getInstance().getPlayer();
    }

    public String subtractCommandWord(String userString) {
        String subtracter = getCommandWord();
        int lengthSubtracter = subtracter.length();
        String finalString = userString.substring(lengthSubtracter);
        return finalString;
    }

    @Override
    public void init(String userString) {
        this.addedRoomName = subtractCommandWord(userString).trim();
    }

    @Override
    public boolean execute() {
        player.addNeighbor(addedRoomName);
        return false;
    }

    @Override
    public String getCommandWord() {
        return "add room";
    }
}

public interface Observer {
    public void handleRoomMoveNotification(Graph.Node room);

}

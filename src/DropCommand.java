public class DropCommand implements Command {
    private Player player;
    private String itemName;


    public DropCommand() {
        this.player = Graph.getInstance().getPlayer();
    }

    public String subtractCommandWord(String userString) {
        String subtracter = getCommandWord();
        int lengthSubtracter = subtracter.length();
        String finalString = userString.substring(lengthSubtracter);
        return finalString;
    }


    @Override
    public void init(String userString) {
        this.itemName = subtractCommandWord(userString).trim();
    }

    @Override
    public boolean execute() {
        Item item = player.items.removeItem(itemName);
        if (item == null) {
            System.out.println("You do not have: " + itemName);
        } else {
            player.getRoom().addItem(item);
            System.out.println("You have dropped the: " + itemName);
        }
        return false;
    }

    @Override
    public String getCommandWord() {
        return "drop";
    }
}

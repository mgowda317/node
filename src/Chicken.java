public class Chicken extends GenericEntity {
    public void move() {
        randomMove();
    }

    public Chicken(String name, String description, Graph.Node currentRoom) {
        super(currentRoom);
        setName(name);
        setDescription(description);
    }
}
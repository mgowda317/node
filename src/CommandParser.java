import java.util.Dictionary;
import java.util.Hashtable;

public class CommandParser {

    private Dictionary<String, Command> commands;

    public CommandParser() {
        commands = new Hashtable<String, Command>();
    }

    public void addCommand(Command c) {
        commands.put(c.getCommandWord(), c);
    }

    public Command parseCommandString(String userString) {
        String parserKey;
        if (userString.isEmpty()) {
            return null;
        }

        String[] commandPart = userString.split(" ");

        if (userString.startsWith("add ")) {
            parserKey = commandPart[0] + " " + commandPart[1];
        } else {
            parserKey = commandPart[0];
        }
        Command command = commands.get(parserKey);
        if (command == null) {
            command = commands.get("print");
        }
        command.init(userString);
        return command;
    }
}

import java.util.ArrayList;
import java.util.List;

public class Graph {
    private List<Node> nodes;
    private EntityContainer entities;
    private static Graph instance;
    private Player player;

    private Graph() {
        nodes = new ArrayList<Node>();
        entities = new EntityContainer();

    }

    public void setPlayer(Player player){
        this.player = player;
    }

    public Player getPlayer(){
        return player;
    }

    public static Graph getInstance() {
        if (instance == null) {
            instance = new Graph();
        }

        return instance;
    }


    public void moveAllEntities() {
        for (Entity e : entities.getEntities()) {
            e.move();
        }
    }

    public void addEntity(Entity entity) {
        entities.addEntity(entity);
    }

    public void addNode(String name) {
        Node n = new Node(name);
        nodes.add(n);

    }

    public void addDirectedEdge(String name1, String name2) {
        Node n1 = getNode(name1);
        Node n2 = getNode(name2);
        n1.addNeighbor(n2);
    }

    public void addUndirectedEdge(String name1, String name2) {
        addDirectedEdge(name1, name2);
        addDirectedEdge(name2, name1);
    }

    public Node getNode(String name) {
        for (Node n : nodes) {
            if (n.getName().equals(name)) {
                return n;
            }
        }
        return null;

    }


    public class Node {
        private String name;
        private ArrayList<Node> neighbors;
        private ItemContainer items;

        public void addItem(Item item) {
            items.addItem(item);
        }

        public void addItem(String name) {
            items.addItem(name);
        }

        public Item removeItem(String name) {
            return items.removeItem(name);
        }

        private Node(String name) {
            this.name = name;
            neighbors = new ArrayList<Node>();
            items = new ItemContainer();
        }

        public List<Entity> getEntities() {
            ArrayList<Entity> roomEntities = new ArrayList<Entity>();
            for (Entity e : entities.getEntities()) {
                if (e.getRoom().equals(this)) {
                    roomEntities.add(e);
                }
            }
            return roomEntities;
        }

        public void addNeighbor(Node n) {
            neighbors.add(n);
        }

        public void addNeighbor(String name) {
            neighbors.add(new Node(name));
        }


        public String getNeighborNames() {
            String result = "[";
            if (neighbors.size() > 0) {
                result += neighbors.get(0).getName();
            }
            for (int neighborIndex = 1; neighborIndex < neighbors.size(); neighborIndex++) {
                Node neighbor = neighbors.get(neighborIndex);
                result += ", " + neighbor.getName();
            }
            return result + "]";
        }

        public ArrayList<Node> getNeighbors() {
            return neighbors;
        }

        public Node getNeighbor(String name) {
            for (Node neighbor : neighbors) {
                if (neighbor.getName().equals(name)) {
                    return neighbor;
                }
            }
            return null;
        }

        public String getName() {
            return name;
        }

    }
}

public class InventoryCommand implements Command {
    private Player player;

    public InventoryCommand() {
        this.player = Graph.getInstance().getPlayer();
    }


    @Override
    public void init(String userString) {

    }

    @Override
    public boolean execute() {
        System.out.println( "your inventory: " + player.getInventoryString());
        return false;
    }

    @Override
    public String getCommandWord() {
        return "inventory";
    }
}

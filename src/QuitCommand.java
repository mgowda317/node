public class QuitCommand implements Command {
    private Player player;

    public QuitCommand() {
        this.player = Graph.getInstance().getPlayer();
    }

    @Override
    public void init(String userString) {

    }

    @Override
    public boolean execute() {
        return false;
    }

    @Override
    public String getCommandWord() {
        return "quit";
    }
}

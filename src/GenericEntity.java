import java.util.ArrayList;
import java.util.Random;

public abstract class GenericEntity implements Entity {
    private String name, description;
    private Graph.Node currentRoom;

    public GenericEntity(Graph.Node currentRoom) {
        setRoom(currentRoom);
    }

    public Graph.Node getRoom() {
        return currentRoom;
    }

    public void setRoom(Graph.Node node) {
        currentRoom = node;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public abstract void move();

    public void randomMove() {
        ArrayList<Graph.Node> neighbors = getRoom().getNeighbors();
        int neighborsCount = neighbors.size();
        if (neighborsCount == 0) {
            return;
        } else {
            Random rand = new Random();
            int neighborsIndex = rand.nextInt(neighborsCount);
            Graph.Node nextRoom = neighbors.get(neighborsIndex);
            setRoom(nextRoom);
        }
    }
}

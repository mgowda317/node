import java.util.ArrayList;
import java.util.List;

public class ItemContainer {
    public List<Item> items;

    public ItemContainer() {
        items = new ArrayList<Item>();
    }


    public void addItem(String name) {
        addItem(name, "");
    }

    public void addItem(String name, String desc) {
        addItem(new Item(name, desc));
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public Item removeItem(String name) {
        for (int index = 0; index < items.size(); index++) {
            Item item = items.get(index);
            if (item.getName().equals(name)) {
                items.remove(index);
                return item;
            }
        }
        return null;
    }

    public String getInventoryString() {
        String result = "[";
        if (items.size() > 0) {
            result += items.get(0).getName();
        }
        for (int itemsIndex = 1; itemsIndex < items.size(); itemsIndex++) {
            Item item = items.get(itemsIndex);
            result += ", " + item.getName();
        }
        return result + "]";
    }

    public void displayInventory() {
        System.out.print("inventory:");
        System.out.print(getInventoryString());
    }

    public String displayItems() {
        String result = " [";
        if (items.size() > 0) {
            result += items.get(0).getName();
        }
        for (int itemsIndex = 1; itemsIndex < items.size(); itemsIndex++) {
            Item item = items.get(itemsIndex);
            result += ", " + item.getName();
        }
        return result + "]";
    }

    public boolean contains(String name) {
        for (Item item : items) {
            String itemName = item.getName();
            if (itemName.equals(name)) {
                return true;
            }
        }
        return false;
    }
}

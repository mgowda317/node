
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Graph g = Graph.getInstance();
        CommandParser parser = new CommandParser();

        g.addNode("hall");
        g.addNode("closet");
        g.addNode("bedroom");
        g.addNode("dungeon");
        g.addNode("cell");
        g.addNode("stairs");

        g.addDirectedEdge("bedroom", "cell");
        g.addDirectedEdge("stairs", "hall");

        g.addUndirectedEdge("hall", "bedroom");
        g.addUndirectedEdge("hall", "closet");
        g.addUndirectedEdge("bedroom", "dungeon");
        g.addUndirectedEdge("dungeon", "stairs");


        Graph.Node hall = g.getNode("hall");
        hall.addItem("hair tie");
        hall.addItem("pictures");
        hall.addItem("books");
        Graph.Node closet = g.getNode("closet");
        closet.addItem("pants");
        closet.addItem("shirts");
        closet.addItem("wumpus tracker");
        Graph.Node bedroom = g.getNode("bedroom");
        bedroom.addItem("pillow");
        bedroom.addItem("sheets");
        bedroom.addItem("pen");
        Graph.Node dungeon = g.getNode("dungeon");
        dungeon.addItem("spider web");
        dungeon.addItem("sword");
        Graph.Node stairs = g.getNode("stairs");
        stairs.addItem("bow and arrow");
        stairs.addItem("quiver");
        Graph.Node cell = g.getNode("stairs");
        cell.addItem("loneliness");

        Player player = new Player("danny", "that one guy", hall);
        g.setPlayer(player);

        Popstar popstar = new Popstar("Pake Jaul", "a very nice fellow", closet);
        g.addEntity(popstar);

        Wumpus wumpus = new Wumpus("Most Palone", "Rockstar", stairs);
        g.addEntity(wumpus);

        Chicken chicken = new Chicken("zoe", "the cat", bedroom);
        g.addEntity(chicken);

        initCommands(parser);
        wumpus.addObserver(player);

        String response = "";
        Scanner in = new Scanner(System.in);
        printMenu();
        do {
            System.out.println("You are currently in the " + player.getRoom().getName());
            System.out.println("What do you want to do ? >");
            response = in.nextLine();
            Command command = parser.parseCommandString(response);
            if (command == null) {
                continue;
            }
            command.execute();
            g.moveAllEntities();
        } while (!response.equals("quit"));
    }

    private static void initCommands(CommandParser parser) {
        parser.addCommand(new GoCommand());
        parser.addCommand(new LookCommand());
        parser.addCommand(new TakeCommand());
        parser.addCommand(new InventoryCommand());
        parser.addCommand(new AddCommand());
        parser.addCommand(new DropCommand());
        parser.addCommand(new QuitCommand());
        parser.addCommand(new PrintMenuCommand());

    }


    public static void printMenu() {
        System.out.println("Your commands are:");
        System.out.println("go <room name>");
        System.out.println("look");
        System.out.println("add room <room name>");
        System.out.println("commands");
        System.out.println("take <item name>");
        System.out.println("drop <item name>");
        System.out.println("quit");
    }

}